/**
 * These rules are specific to JavaScript running on Node.js
 * @module rules/best-practices
 * @see http://eslint.org/docs/rules/#best-practices
 */

'use strict';

module.exports = {
	rules: {
		'dot-notation': ['error', { 'allowPattern': '^[a-z]+(_[a-z]+)+$' }],
		'eqeqeq': ['error', 'smart']
	}
};
