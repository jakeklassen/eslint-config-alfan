/**
 * These rules are purely matters of style and are quite subjective.
 * @module rules/stylistic-issues
 * @see http://eslint.org/docs/rules/#stylistic-issues
 */

'use strict';

module.exports = {
	rules: {
		'camelcase': 'error',
		'indent': ['error', 'tab', { 'SwitchCase': 1 }],
		'linebreak-style': ['error', 'unix'],
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'space-before-function-paren': ['error', 'never']
	}
};
