/**
 * These rules are specific to JavaScript running on Node.js
 * @module rules/node
 * @see http://eslint.org/docs/rules/#nodejs-and-commonjs
 */

'use strict';

module.exports = {
	rules: {
		// Enforces error handling in callbacks (off by default) (on by default in the node environment)
		'handle-callback-err': ['error', '^.*(e|E)rr(or)?$']
	}
};
