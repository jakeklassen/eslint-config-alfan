/**
 * ECMAScript-6 configuration.
 * @module configurations/es6
 */

'use strict';

const deepExtend = require('deep-extend');

module.exports = deepExtend(
	{
		env: {
			es6: true
		},
		extends: 'eslint:recommended',
		parserOptions: {
			ecmaVersion: 6,
			sourceType: 'module'
		},
		rules: {}
	},
	require('../rules/node.js'),
	require('../rules/stylistic-issues.js')
);
