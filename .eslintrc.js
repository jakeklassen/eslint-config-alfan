'use strict';

const deepExtend = require('deep-extend');
const configuration = require('./configurations/es6.js');

module.exports = deepExtend(configuration, {
	env: {
		node: true
	}
});
